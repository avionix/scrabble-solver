# Scrabble Solver

A TUI application written in Rust that produces the best word given the board state and available letters. Future versions may introduce strategic play, rather than going for the best point value per turn.

## Basic architecture

- Application runs a [database](https://lib.rs/crates/sled) containing all [≈475k English words](https://wordnet.princeton.edu/) associated with:
	- The length of the word
	- The number of occurrences of each character in the word
	- The first letter
- The board state is maintained as a 2D array and displayed graphically.
- The application iterates across the board from the top left to the bottom right.
	- It checks if there's room to start a word (it won't try to start a word inside another word)
	- It checks horizontal before vertical words.
	- If there isn't room, it skips the check.
	- If there _is_ room, it proceeds on.
	- If there were preceding letters, it adds them to a list of 'static' letters.
- It searches the database for words of increasing length.
	- Starting first at 2 letters, then increasing.
	- The search is narrowed down based on what words fit in the letters available.
	- Available letters include the rack and any board letters occluded by the word length.
	- It further narrows down the search by checking 'static' letters against the words produced by the database.
- It calculates the point value of the produced word and stores it in a data structure.
- When it has exhausted all possibilities for words in that spot, it proceeds on to the next.
- Once it has checked the entire board, it sorts the produced words by point count and displays the top 10.

#### Architecture notes

- Parallel operation will be used wherever possible. Database searches and iteration through the board will be fully parallelized.
- Highlighting a produced word in the TUI will show where it goes on the board.
- To manually enter a word:
	- A cursor is moved to the correct tile.
	- Either H or V is pressed to indicate a horizontal or vertical word.
	- The word is typed in.
	- Pressing Enter saves it.

#### Future development
##### Strategic play
- Strategic play will produce player ratings based on various criteria.
- Given aggressiveness settings, it will attempt to increase the difficulty for select players to make a move.
- The application will 'count cards' to generate the probabilities of a player receiving particular letters from the bag.
- It will attempt to simulate future moves using the player ratings.

## Official game rules
> You should have a game board, 100 letter tiles, a letter bag, and four racks.
> 
> Before the game begins, all players should agree upon the dictionary that they will use, in case of a challenge. All words labeled as a part of speech (including those listed of foreign origin, and as archaic, obsolete, colloquial, slang, etc.) are permitted with the exception of the following: words always capitalized, abbreviations, prefixes and suffixes standing alone, words requiring a hyphen or an apostrophe.
> 
> Place all letters in the pouch, or facedown beside the board, and mix them up. Draw for first play. The player with the letter closest to "A" plays first. A blank tile beats any letter. Return the letters to the pool and remix. All players draw seven new letters and place them on their racks.
